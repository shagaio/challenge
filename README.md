<h1 align="center">Next.js SSR app on Cloud Functions for Firebase with Firebase Hosting</h1>

## Why?

Host your SSR Next.js app on Cloud Functions enabling a low-cost, auto-scaling SSR app experience leveraging Firebase's sweet developer experience.

## Important!

We have simulated backend approval workflow in background as a google function that listen on change to create record in database


## Installation and Setup for local development

Make sure you installed npm and yarn

```bash
yarn install
yarn dev
```