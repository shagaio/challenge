import { nextApp as next, statusUpdate } from './app/app';

// SSR Next.js app Cloud Function used by Firebase Hosting
// yarn deploy-app
const app = {
	next
	// other Hosting dependencies
};


export { app, statusUpdate };
