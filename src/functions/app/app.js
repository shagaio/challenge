import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import next from 'next';
admin.initializeApp();
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev, conf: { distDir: 'next' } });
const handle = app.getRequestHandler();
import _ from 'lodash';

const nextApp = functions.https.onRequest((request, response) => {
	return app.prepare().then(() => handle(request, response));
});
const deplay = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
const statusUpdate = functions.firestore.document(`cards/{id}`).onCreate( async (snapshot, context) => {
	let reference = admin.firestore().doc(`cards/${snapshot.id}`);
	// Simulate approval workflow
	await deplay(10000);
	await reference.update({
		currentWorkflow: 'pending'
	})
	
})
export { nextApp, statusUpdate };
