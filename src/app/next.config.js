const withSass = require('@zeit/next-sass');
module.exports = withSass({
	distDir: '../../dist/functions/next',
	publicRuntimeConfig: {
		// Will be available on both server and client
		staticFolder: '/static'
	}
});
