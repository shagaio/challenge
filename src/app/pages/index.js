import '../static/styles.scss';
import Head from 'next/head';
import App from '../components/App';
import Link from 'next/link';
import { Text, Footer } from '../components/Common';
export default () => {
	return (
		<div>
			<Head>
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
				/>
				<meta charSet="utf-8" />
			</Head>
			<style jsx global>{`
				* {
					box-sizing: border-box;
					margin: 0;
					padding: 0;
				}
				html,
				body {
					font-family: "Roboto", sans-serif;
					font-size: 14px;
					line-height: 20px;
				}
				.svg-inline--fa {
					height: 1em;
					width: 0.875em;
				}
			`}</style>
			<App />
			<Footer>
				<Link href="/design">
					<a>
						<Text>Design System</Text>
					</a>
				</Link>
			</Footer>
		</div>
	);
};
