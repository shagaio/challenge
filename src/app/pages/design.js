import '../static/styles.scss';
import Head from 'next/head';
import Link from 'next/link';
import {
	Container,
	Button,
	Grid,
	Text,
	DropDown,
	DropDownItem,
	Card,
	Image,
	Header,
	Input,
	DatePicker,
	ProgressBar,
	SearchInput
} from '../components/Common';
import { faEdit, faTrash, faCheckCircle, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
export default () => {
	return (
		<div>
			<Head>
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
				/>
				<meta charSet="utf-8" />
			</Head>
			<Header>
				<Link href="/">
					<a>
						<Text size="large" type="primary">
							Home
						</Text>
					</a>
				</Link>
			</Header>
			<Container>
				<div className="mt2">
					<Grid column={4}>
						<DropDown label="Normal" value="Example Item 2">
							<DropDownItem>Item 1</DropDownItem>
							<DropDownItem selected={true}>Example Item 2</DropDownItem>
							<DropDownItem>Item 3</DropDownItem>
						</DropDown>
						<DropDown label="Dropdown items with icon" value="test" icon={faCheckCircle}>
							<DropDownItem icon={faEdit}>Item 1</DropDownItem>
							<DropDownItem icon={faCheckCircle} selected={true}>
								Example Item 2
							</DropDownItem>
							<DropDownItem icon={faTrash}>Item 3</DropDownItem>
						</DropDown>
						<DropDown label="Icon dropdown" icon={faPencilAlt}>
							<DropDownItem>Item 1</DropDownItem>
							<DropDownItem selected={true}>Example Item 2</DropDownItem>
							<DropDownItem>Item 3</DropDownItem>
						</DropDown>
					</Grid>
				</div>
				<div className="mt4">
					<Grid column={4}>
						<Text size="large" type="primary">
							Text large primary
						</Text>
						<Text size="large" type="alert">
							Text large alert
						</Text>
						<Text size="large" type="warning">
							Text large alert
						</Text>
						<Text size="large">Text large normal</Text>
						<Text size="normal" type="primary">
							Text normal primary
						</Text>
						<Text size="normal" type="alert">
							Text normal alert
						</Text>
						<Text size="normal" type="warning">
							Text normal alert
						</Text>
						<Text size="normal">Text normal normal</Text>
						<Text size="small" type="primary">
							Text small primary
						</Text>
						<Text size="small" type="alert">
							Text small alert
						</Text>
						<Text size="small" type="warning">
							Text small alert
						</Text>
						<Text size="small">Text small normal</Text>
					</Grid>
				</div>
				<div className="mt4">
					<Button type="primary">Primary</Button>
					<Button type="red">Secondary</Button>
					<Button type="secondary">Secondary</Button>
					<Button type="warning">Warning</Button>
					<Button>Normal</Button>
					<Button disabled={true}>Disabled</Button>
				</div>
				<div className="mt4">
					<Grid column={4}>
						<Card>
							<Image url="https://uploads-ssl.webflow.com/5aa579da80d0970001ed57da/5c1d585e9279b375df22d24a_uijar.png" />
							<div className="pa4">
								<Text size="large" type="primary">
									Normal
								</Text>
							</div>
						</Card>
						<Card dashed={true}>
							<div className="pa4">
								<Text size="large" type="primary">
									Dashed
								</Text>
							</div>
						</Card>
						<Card disabled={true}>
							<div className="pa4">
								<Text size="large" type="primary">
									Disabled
								</Text>
							</div>
						</Card>
						<Card loading={true}>
							<div className="pa4">
								<Text size="large" type="primary">
									Disabled
								</Text>
							</div>
						</Card>
					</Grid>
				</div>
				<div className="mt4">
					<Grid column={4}>
						<Input label="First name" placeholder="Please enter your name" />
						<Input label="Disabled Input" disabled={true} placeholder="Please enter your name" />
					</Grid>
					<Grid column={4}>
						<Input label="Error state" error="Please enter your name" />
					</Grid>
					<Grid column={4}>
						<div>
							<label>Search bar</label>
							<div className="h-3 mt1">
								<SearchInput />
							</div>
						</div>
					</Grid>
				</div>
				<div className="mt4 t-l">
					<Grid column={4}>
						<div>
							<label>Date picker</label>
							<div className="mt1">
								<DatePicker date={new Date()} />
							</div>
						</div>
						<div>
							<label>Progress bar</label>
							<div className="mt1">
								<ProgressBar progress={40} />
							</div>
						</div>
					</Grid>
				</div>
			</Container>
		</div>
	);
};
