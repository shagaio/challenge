import React, { useContext, useState } from 'react';
import { Card, Image, Text, Currency, Grid, ProgressBar, Hidden, DropDown, DropDownItem } from './Common';
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faDatabase,
	faUserFriends,
	faEye,
	faEdit,
	faTrash,
	faCheckCircle,
	faShareAlt,
	faFileSignature,
	faTimes,
	faPauseCircle,
	faBan,
	faStopwatch
} from '@fortawesome/free-solid-svg-icons';
import { AppDataContext } from '../hooks';
import { updateStatus, shareService, deleteService } from '../api';
const StateMap = {
	saved: [ 'pending' ],
	pending: [ 'declined', 'active' ],
	active: [ 'paused', 'terminated', 'expired' ],
	paused: [ 'active' ]
};
const deplay = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));
const StateConfig = [
	{
		name: 'saved',
		action: 'create'
	},
	{
		name: 'pending',
		action: 'submit for review',
		icon: faFileSignature
	},
	{
		name: 'declined',
		action: 'reject',
		label: 'rejected',
		icon: faTimes
	},
	{
		name: 'active',
		action: 'publish',
		label: 'live',
		icon: faCheckCircle
	},
	{
		name: 'expired',
		action: 'expire',
		icon: faStopwatch
	},
	{
		name: 'paused',
		action: 'pause',
		icon: faPauseCircle
	},
	{
		name: 'terminated',
		action: 'terminate',
		icon: faBan
	}
];
const progressMap = {
	expired: 100,
	active: 100
};
export const Status = ({ currentWorkflow }) => {
	const colorMap = {
		declined: 'b-alert',
		active: 'b-success'
	};
	return <span className={`circle ${colorMap[currentWorkflow] || 'b-warning'}  `} />;
};
export const Service = (service) => {
	let {
		id,
		primaryMediaUrl,
		currentWorkflow,
		cardTitle,
		listOfPlans,
		totalRevenue,
		views,
		shares,
		cardDescription
	} = service;

	let { setSelected, filter, query } = useContext(AppDataContext);
	let status = _.find(StateConfig, { name: currentWorkflow }) || {};
	let disabled = !_.isMatchWith(service, filter);
	if (query && !disabled) {
		disabled = JSON.stringify(service).toLowerCase().indexOf(query.toLowerCase()) == -1;
    }
    let [loading, setLoading] = useState(false);
    const update = (next) => {
        return async () => {
            setLoading(true);
            await deplay(4000);
            await next()
            setLoading(false);
        }
    }
	return (
		<Card disabled={disabled} loading={loading}>
			<div className="relative">
				<Image url={primaryMediaUrl} height={6} className="relative pa2 t-r">
					<div className="c-warning inline-block b-white smooth pa1 capitalize t-l">
						<DropDown icon={faEdit} right="true">
							<DropDownItem onClick={() => setSelected(service)} icon={faEdit}>
								Edit
							</DropDownItem>
							{_.chain(StateMap[currentWorkflow])
								.map((status) => {
									return {
										..._.find(StateConfig, { name: status }),
										status
									};
								})
								.map(({ name, icon, action, status }) => {
									return (
										<DropDownItem key={name} onClick={update(() => updateStatus(id, status))} icon={icon}>
											{action || name}
										</DropDownItem>
									);
								})
								.value()}
							<DropDownItem onClick={() => shareService(id)} icon={faShareAlt}>
								Share
							</DropDownItem>
							<DropDownItem onClick={() => deleteService(id)} icon={faTrash}>
								Delete
							</DropDownItem>
						</DropDown>
					</div>
				</Image>
				<div className="pa2">
					<div>
						<Text type="primary">{cardTitle || 'No title'}</Text>
					</div>
					<div className="mt1">
						<Text size="small">{cardDescription || 'No description '}</Text>
					</div>
					<div className="mt1">
						<Grid column={2} mobile={true}>
							<Text size="small">
								{listOfPlans &&
									listOfPlans.map(({ price }) => {
										return <Currency key={price} {...price} />;
									})}
							</Text>
							<div className="t-r">
								<div className="ba capitalize t-small inline-block smooth ph1">
									{(status && status.label) || status.name}&nbsp;
									<Status currentWorkflow={currentWorkflow} />
								</div>
							</div>
						</Grid>
					</div>
					<div className="mt2">
						<ProgressBar progress={progressMap[currentWorkflow] || 0} />
					</div>
				</div>
				{currentWorkflow == 'declined' && (
					<Grid column={2} mobile={true} className="b-secondary pv1 ph2">
						<div className="c-red">Card is on hold</div>
						<div className="t-r pointer"  onClick={() => updateStatus(id, 'saved')}  >Resubmit</div>
					</Grid>
				)}
				{currentWorkflow != 'declined' && (
					<Grid column={3} mobile={true} className="b-secondary t-small pv1 ph2">
						<Hidden hidden={currentWorkflow == 'saved'}>
							<FontAwesomeIcon icon={faDatabase} />
							{` $${Number(totalRevenue).toLocaleString('en-US', { currency: 'USD' })}`}
						</Hidden>
						<Hidden hidden={currentWorkflow == 'saved'} className="t-c">
							<FontAwesomeIcon icon={faUserFriends} />
							{` ${shares}`}
						</Hidden>
						<Hidden hidden={currentWorkflow == 'saved'} className="t-r">
							<FontAwesomeIcon icon={faEye} />
							{` ${views}`}
						</Hidden>
					</Grid>
				)}
			</div>
		</Card>
	);
};
