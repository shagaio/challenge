import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faCalendar, faChevronLeft, faChevronRight, faSearch } from '@fortawesome/free-solid-svg-icons';
import Moment from 'react-moment';
import moment from 'moment';

export const Container = ({ children, style }) => {
	return (
		<div className="container" style={style}>
			{children}
		</div>
	);
};

export const Grid = ({ children, column, mobile, className, style }) => {
	return (
		<div className={`grid ${column ? 'column' + column : 'auto'} ${mobile && 'mobile-grid'} ${className}`} style={style}>
			{children}
		</div>
	);
};

export const Header = ({ children }) => {
	return (
		<div className="b-secondary pv2">
			<Container>{children}</Container>
		</div>
	);
};
export const Hidden = ({ hidden, children, className }) => {
	return <div className={`animated ${hidden && 'hidden'} ${className}`}>{children}</div>;
};
export const DropDownItem = (props) => {
	let { children, selected, icon } = props;
	return (
		<div {...props} style={{ width: '100%' }} className={`pv1 ph2 b-t ${selected && 'b-warning'}`}>
			{icon && (
				<span className="mr1">
					<FontAwesomeIcon size="lg" icon={icon} />
				</span>
			)}
			{children}
		</div>
	);
};
export const Currency = ({ amount, currencySymbol }) => {
	return `${currencySymbol} ${new Number(amount).toLocaleString('en-US', { currency: 'USD' })} `;
};
export const DropDown = (props) => {
	let { children, value, icon, right, className, label } = props;
	let [ open, setOpen ] = useState(false);
	const node = useRef();
	let eventHandler = (e) => {
		if (!node) {
			return;
		}
		let inside = node.current.contains(e.target);
		if (inside) {
			setOpen((open) => !open);
			return;
		}
		setOpen(false);
	};
	useEffect(() => {
		document.addEventListener('click', eventHandler, false);
		return () => {
			document.removeEventListener('click', eventHandler, false);
		};
	}, []);
	return (
		<div>
			{label && (
				<div className="mb1">
					<label>{label}</label>
				</div>
			)}
			<div>
				<div className="dropdown relative" style={{ display: 'inline-block' }} ref={node}>
					<div>
						<div {...props} className={`${value && 'btn'} ${className}`}>
							{value && <span className="mr1">{value}</span>}
							<FontAwesomeIcon icon={icon || faChevronDown} />
						</div>
						<Hidden hidden={!open}>
							<div className={`absolute ${right && 'right'}`}>
								<div className={`dropdown ba mt2 b-secondary`}>{children}</div>
							</div>
						</Hidden>
					</div>
				</div>
			</div>
		</div>
	);
};
export const SearchInput = ({ onChange }) => {
	let [ active, setActive ] = useState(false);
	let [ query, setQuery ] = useState();
	const change = (e) => {
        setQuery(e.target.value);
        onChange &&  onChange(e.target.value);
	};
	return (
		<div className={`search-wrapper ${active && 'active'}`}>
			<div className="">
                <span className="icon">
				    <FontAwesomeIcon icon={faSearch} />&nbsp;&nbsp;
                </span>
				<input
                    type="search"
                    onChange={change}
					onFocus={() => setActive(true)}
					onBlur={() => query == '' && setActive(false)}
				/>
			</div>
		</div>
	);
};

export const Button = (props) => {
	let { children, className, type, disabled, icon } = props;
	if (icon) {
		return (
			<div {...props} className={`btn b-${type} ${disabled && 'disabled'} icon ${className}`}>
				<FontAwesomeIcon icon={icon}>{children}</FontAwesomeIcon>
			</div>
		);
	}
	return (
		<button {...props} className={`btn b-${type} ${disabled && 'disabled'} ${className}`}>
			{children}
		</button>
	);
};
export const Spinner = () => {
	return (
		<div className=" h-3 w-3 relative">
			<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" className="spinner">
				<ellipse cx="12" cy="12" rx="10" ry="10" className="bs-Spinner-ellipse" />
			</svg>
		</div>
	);
};
export const Blocker = () => {
	return (
		<div className="blocker">
			<Spinner />
		</div>
	);
};
export const Text = (props) => {
	let { size, type, children, align } = props;
	return <span className={`c-${type} t-${align && align[1]} t-${size}`}>{children}</span>;
};

export const Card = (props) => {
	let { disabled, dashed, children, loading } = props;
	if (loading) {
		return (
			<div className="card t-c dashed pointer h-100 c-content">
				<Spinner />
			</div>
		);
	}
	return <div className={`card ${disabled && 'disabled'} ${dashed && 'dashed'}`}>{!loading && children}</div>;
};

export const Image = ({ url, height, children, className }) => {
	return (
		<div
			className={`image-holder h-${height} ${className}`}
			style={{
                backgroundImage: `url('${url}')`,
                backgroundPosition: 'center'
			}}
		>
			{children}
		</div>
	);
};

export const Input = (props) => {
	let { label, placeholder, type, disabled, value, error } = props;
	return (
		<div className={`${error && 'error'}`}>
			<label>{label}</label>
			<div className={`${disabled && 'disabled'} mt1 `}>
				<input {...props} type={type || 'text'} placeholder={placeholder || ''} value={value} />
			</div>
			<div className={`mt1`}>
				<Text size="small">{error}</Text>
			</div>
		</div>
	);
};
export const ProgressBar = ({ progress }) => {
	return (
		<div className="progress-bar b-secondary-dark">
			<div className="b-warning" style={{ width: `${progress || 0}%` }} />
		</div>
	);
};
export const DatePicker = ({ date, onChange }) => {
	const calendarStrings = {
		sameDay: '[Today]',
		nextDay: '[Tomorrow]',
		nextWeek: 'dddd',
		lastDay: '[Yesterday]',
		lastWeek: '[Last] dddd',
		sameElse: 'YYYY'
	};
	let [ selected, setSelected ] = useState(date);
	const update = (by) => () => {
		let date = moment(selected).add(by, 'd');
		setSelected(date);
		onChange && onChange(date);
	};
	return (
		<Grid column={4} className="date-picker no-gap c-red">
			<Button icon={faChevronLeft} onClick={update(-1)} />
			<div>
				<div className="pointer" onClick={update('-1')} />
				<FontAwesomeIcon icon={faCalendar} />&nbsp;&nbsp;
				<Moment calendar={calendarStrings}>{selected}</Moment>&nbsp;
				<Moment format="MMM Do">{selected}</Moment>&nbsp;&nbsp;
				<span className="pointer" onClick={update('1')} />&nbsp;&nbsp;
			</div>
			<Button icon={faChevronRight} onClick={update('1')} />
			<div>
				<div className="badge">
					<Moment diff={new Date()} unit="days">
						{selected}
					</Moment>{' '}
					days
				</div>
			</div>
		</Grid>
	);
};
export const Footer = ({ children }) => {
	return (
		<div>
			<div className="container b-t pv2">{children}</div>
		</div>
	);
};
