import React from 'react';
import Header from './Header';
import { ServicesListView } from './ServicesListView';
import { AppDataContextProvider } from '../hooks';


const App = () => (
	<AppDataContextProvider>
		<main>
			<Header />
			<ServicesListView />
		</main>
	</AppDataContextProvider>
);

export default App;
