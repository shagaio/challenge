import React, { useContext, useState } from 'react';
import { AppDataContext } from '../hooks';
import {
	CreateService,
	UpdateService,
} from './Form';
import { Service } from './Service';
import { Card, Grid } from './Common';

export const ServicesListView = () => {
	let { cards, loading, selected } = useContext(AppDataContext);

	return (
		<div className="container pv4">
			{!selected && (
				<Grid column={4}>
					{cards && cards.map(card => <Service key={card.id} {...card}/>)}
					{loading && <Card loading={loading} />}
					<CreateService />
				</Grid>
			)}
			<UpdateService />
		</div>
	);
};
