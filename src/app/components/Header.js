import React, { useContext } from 'react';
import filters from '../static/filters';
import { DropDown, DropDownItem, IconDropDown } from './DropDown';
import { AppDataContext } from '../hooks';
import { faList } from '@fortawesome/free-solid-svg-icons';
import { DatePicker, SearchInput } from './Common';
import _ from 'lodash';

export default () => {
	let { filter, setFilter, campaigns, date, setDate, setQuery } = useContext(AppDataContext);
	let { campaignId, currentWorkflow } = filter || {};
	let { campaignName } = _.find(campaigns, { id : campaignId}) || {};
	return (
		<header className="b-secondary pv2 ">
			<div className="container grid column2" style={{ alignItems: 'center' }}>
				<div>
					<DropDown value={`${ campaignName || 'All campaigns'} `}>
						<DropDownItem onClick={() => setFilter({ ..._.omit(filter, 'campaignId') })} selected={!campaignId}>All campaigns</DropDownItem>
						{campaigns.map((campaign) => {
							return (
								<DropDownItem key={campaign.id} onClick={() => setFilter({ ...filter, campaignId: campaign.id })}>
									{campaign.campaignName}
								</DropDownItem>
							);
						})}
					</DropDown>
					<DropDown icon={faList}>
						<DropDownItem onClick={() => setFilter({ ..._.omit(filter, 'currentWorkflow') })} selected={!currentWorkflow}>Everything</DropDownItem>
						{filters.map((s) => {
							return (
								<DropDownItem key={s} onClick={() => setFilter({ ...filter, currentWorkflow: s })} selected={s == currentWorkflow}>
									{s}
								</DropDownItem>
							);
						})}
					</DropDown> 
					<span className="ml1 capitalize">
						{currentWorkflow || 'Filter by status'}
					</span>
				</div>
				<div className="t-r grid auto" style={{ gridTemplateColumns: '1fr auto'}}>
					<SearchInput onChange={setQuery} />
					<DatePicker date={date} onChange={setDate} />
				</div>
			</div>
		</header>
	);
};
