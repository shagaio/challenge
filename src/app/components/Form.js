import firebase from 'firebase/app';
import React, { useState, useContext, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { AppDataContext } from '../hooks';
import _ from 'lodash';
import { DropDown, DropDownItem } from './DropDown';
import { Grid, Input, Button, Image, Text } from './Common';


export const Price = ({ value: defaultValue, onChange }) => {
	let [ value, setValue ] = useState(defaultValue || {});
	let { amount, currency, currencySymbol } = value;
	const update = (currency) => {
		let updated = { ...value, ...currency };
		setValue(updated);
		onChange && onChange(updated);
	};
	return (
		<div>
			<label>Price</label>
			<div className="mt2">
				<Grid style={{ gridTemplateColumns: 'auto 1fr' }}>
					<DropDown value={`${(currency && `${currencySymbol} ${currency}`) || 'Select currency'}`}>
						<DropDownItem onClick={() => update({ currency: 'JPY', currencySymbol: '¥' })}>
							JPY
						</DropDownItem>
						<DropDownItem onClick={() => update({ currency: 'USD', currencySymbol: '$' })}>
							USD
						</DropDownItem>
					</DropDown>
					<input
						placeholder="Please enter amount"
						value={amount}
						onChange={(e) => update({ amount: e.target.value })}
					/>
				</Grid>
			</div>
		</div>
	);
};
export const ServiceForm = ({ value, setShow }) => {
	let [ card, setCard ] = useState(value || {});
	let { campaigns, setSelected } = useContext(AppDataContext);
	let selectedCampaign = _.find(campaigns, { id: card.campaignId });

	const save = async () => {
		saveService(card);
		setSelected(null);
		setShow && setShow(false);
	};
	const updateEvent = (key) => (e) => {
		return update(key)(e.target.value);
	};
	const update = (key) => (value) => {
		let update = _.set({ ...card }, key, value);
		setCard(update);
	};
	return (
		<Grid column={2}>
			<div>
				<Text size="large">Basic Information</Text>
				<Input
					label="Title"
					placeholder="Please enter title"
					onChange={updateEvent('cardTitle')}
					value={card.cardTitle}
				/>
				<Input
					label="Description"
					placeholder="Please enter description"
					onChange={updateEvent('cardDescription')}
					value={card.cardDescription}
				/>
				<div />
				<div>
					<Input
						label="Media URL"
						placeholder="Please enter media link"
						onChange={updateEvent('primaryMediaUrl')}
						value={card.primaryMediaUrl}
					/>
					<Image url={card.primaryMediaUrl} height={6} />
				</div>
				<div />
				<div>
					<Price value={_.property('listOfPlans.0.price')(card)} onChange={update('listOfPlans.0.price')} />
				</div>
				<div />
				<div>
					<label>Campaign</label>
					<div className="mt1">
						<DropDown
							value={`${(selectedCampaign && selectedCampaign.campaignName) || 'Select campaigns'} `}
						>
							{campaigns.map((campaign) => {
								return (
									<DropDownItem
										key={campaign.id}
										onClick={() => update('campaignId')(campaign.id)}
										selected={campaign.id == card.campaignId}
									>
										{campaign.campaignName}
									</DropDownItem>
								);
							})}
						</DropDown>
					</div>
				</div>
			</div>
			<div>
				<Text size="large">Address</Text>
				<Input
					label="Address Line"
					placeholder="Please enter address Line"
					onChange={updateEvent('locations[0].description')}
					value={_.property('locations[0].description')(card)}
				/>
				<Grid column={3}>

					<Input
						label="City"
						placeholder="Please enter city"
						onChange={updateEvent('locations[0].city')}
						value={_.property('locations[0].city')(card)}
					/>
					<Input
						label="Area"
						placeholder="Please enter area "
						onChange={updateEvent('locations[0].area')}
						value={_.property('locations[0].area')(card)}
					/>
					<Input
						label="Country"
						placeholder="Please enter country "
						onChange={updateEvent('locations[0].country')}
						value={_.property('locations[0].country')(card)}
					/>
				</Grid>
			</div>
			<div>
				<Button type="primary" onClick={save}>
					Save
				</Button>
				<Button onClick={() => setSelected(null)}>Cancel</Button>
			</div>
		</Grid>
	);
};
export const CreateService = () => {
	let { setSelected } = useContext(AppDataContext);
	return (
		<div>
			<div onClick={() => setSelected({})} className="card t-c dashed pointer h-100 c-content">
				<div style={{ justifySelf: 'center' }}>
					<div className="c-secondary-dark">
						<FontAwesomeIcon size="3x" icon={faPlusCircle} />
					</div>
					<div className="mt2">Create a Service Card</div>
				</div>
			</div>
		</div>
	);
};

export const UpdateService = () => {
	let { selected } = useContext(AppDataContext);
	if (!selected) {
		return null;
	}
	return <ServiceForm show={!!selected} value={{ ...selected }} />;
};
