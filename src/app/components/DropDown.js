import React, { useState, useEffect, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

export const DropDownItem = (props) => {
	let { children, selected } = props;
	return (
		<div {...props} style={{ width: '100%' }} className={`pv1 ph2 b-t ${ selected && 'b-warning'}`} >
			{children}
		</div>
	);
};

export const DropDown = (props) => {
    let { children, value, icon, right, className } = props;
    let [ open, setOpen ] = useState(false);
    const node = useRef();
    let eventHandler = (e) => {
        if (!node) {
            return;
        }
        let inside = node.current.contains(e.target);
        if (inside) {
            setOpen((open) => !open);
            return;
        }
        setOpen(false);
    }
    useEffect(() => {
        document.addEventListener('click', eventHandler, false);
        return () => {
            document.removeEventListener('click', eventHandler, false);
        }
    }, [])
	return (
		<div className="dropdown" style={{ display: 'inline-block' }} ref={node}  >
			<div>
				<div  {...props} className={`${value && "btn"} ${className}`}>
					{value}
					<FontAwesomeIcon className="ml1" icon={icon || faChevronDown} />
				</div>
				<div style={{ position: 'relative', display: open ? 'block' : 'none' }}>
					<div
						className="dropdown ba mt2 b-secondary"
						style={{
							transition: 'all 0.2s',
							zIndex: open ? 2 : 0,
							position: 'absolute',
                            opacity: open ? 1 : 0,
                            right: right ? 0 : 'auto'
						}}
					>
						{children}
					</div>
				</div>
			</div>
		</div>
	);
};
