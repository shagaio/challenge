import firebase from 'firebase/app';

export const updateStatus = async (id, status) => {
        await firebase.firestore().doc(`cards/${id}`).update({
            currentWorkflow: status
        });
    
}
export const saveService = async (data) => {
	if (data.id) {
		firebase.firestore().doc(`cards/${data.id}`).set(data);
		return;
	}
	await firebase.firestore().collection('cards').add({
		...data,
		availableQuantity: 0,
		likes: 0,
		shares: 0,
		views: 0,
		subscribers: 0,
		unSubscribers: 0,
		open: 0,
		discard: 0,
		totalRevenue: 0,
		currentWorkflow: 'saved',
		createdAt: firebase.firestore.FieldValue.serverTimestamp(),
		updatedAt: firebase.firestore.FieldValue.serverTimestamp()
	});
};
export const shareService = async (id) => {
	const increment = firebase.firestore.FieldValue.increment(1);
	await firebase.firestore().doc(`cards/${id}`).update({
		shares: increment
	});
};
export const deleteService = async (id) => {
	await firebase.firestore().doc(`cards/${id}`).delete();
};