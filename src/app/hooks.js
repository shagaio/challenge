import React, { createContext, useState, useEffect } from 'react';
import firebase from 'firebase/app';
import 'firebase/firestore';
import { collectionData } from 'rxfire/firestore';
const app =
	!firebase.apps.length &&
	firebase.initializeApp({
		apiKey: 'AIzaSyBiq3gyyJdk_suNrF4ZH78QckqXS2EKSqs',
		authDomain: 'upbound-e999d.firebaseapp.com',
		databaseURL: 'https://upbound-e999d.firebaseio.com',
		projectId: 'upbound-e999d',
		storageBucket: 'upbound-e999d.appspot.com',
		messagingSenderId: '462368931826',
		appId: '1:462368931826:web:7f4871c8e52ddb78'
	});
export const AppDataContext = createContext({
	cards: [],
	campaigns: [],
	loading: true,
	app,
	setFilter: () => {},
	filter: null,
	date: Date.now(),
	setDate: () => {},
	selected: null,
	setSelected: () => {},
	query: null,
	setQuery: () => {}
});

export const AppDataContextProvider = ({ children }) => {
	let [ cards, setCards ] = useState([]);
	let [ campaigns, setCampaigns ] = useState([]);
	let [ filter, setFilter ] = useState(null);
	let [ loading, setLoading ] = useState(true);
	let [ date, setDate ] = useState(Date.now());
	let [ selected, setSelected ] = useState(null);
	let [ query, setQuery ] = useState(null);
	useEffect(() => {
		setLoading(true);
		let query = app.firestore().collection('cards');
		let subscriptionCards = collectionData(query, 'id').subscribe((d) => {
			setLoading(false);
			setCards(d);
		});
		let subscriptionCampaigns = collectionData(app.firestore().collection('campaigns'), 'id').subscribe((d) => {
			setLoading(false);
			setCampaigns(d);
		});
		return () => {
			subscriptionCards.unsubscribe();
			subscriptionCampaigns.unsubscribe();
		};
	}, []);
	return (
		<AppDataContext.Provider
			value={{
				cards,
				filter,
				setFilter,
				loading,
				date,
				setDate,
				selected,
				setSelected,
				campaigns,
				query,
				setQuery
			}}
		>
			{children}
		</AppDataContext.Provider>
	);
};
